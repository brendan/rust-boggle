extern crate rand;

use std::fmt;

use self::rand::distributions::IndependentSample;
use self::rand::distributions::Range;

use dimensions::Dimensions;

pub struct Board {
    dimensions: Dimensions,
    board: Vec<char>,
}

impl Board {
    /// Create a new random board of the given dimensions.
    pub fn new(dimensions: Dimensions) -> Board {
        let mut rng = rand::thread_rng();
        // Range not implemented for char
        let chars = Range::new('a' as u8, 'z' as u8);

        let size = dimensions.rows * dimensions.cols;
        let board = (0..size)
            .map(|_| chars.ind_sample(&mut rng) as char)
            .collect();

        Board { dimensions, board }
    }

    /// returns the number of elements in the board.
    pub fn len(&self) -> usize {
        self.board.len()
    }

    /// returns the index of the board position one row up from start
    /// or None if start is in the first row
    pub fn up(&self, start: usize) -> Option<usize> {
        assert!(start < self.board.len());

        if start >= self.dimensions.cols {
            Some(start - self.dimensions.cols)
        } else {
            None
        }
    }

    pub fn down(&self, start: usize) -> Option<usize> {
        assert!(start < self.board.len());

        if start + self.dimensions.cols < self.board.len() {
            Some(start + self.dimensions.cols)
        } else {
            None
        }
    }

    pub fn left(&self, start: usize) -> Option<usize> {
        assert!(start < self.board.len() as usize);

        if start % self.dimensions.cols != 0 {
            Some(start - 1)
        } else {
            None
        }
    }

    /// Returns the position to the right of the given position
    /// or `None` if the given position is on the right edge of the board.
    pub fn right(&self, start: usize) -> Option<usize> {
        assert!(start < self.board.len() as usize);

        if start + 1 < self.board.len() as usize && (start + 1) % self.dimensions.cols != 0 {
            Some(start + 1)
        } else {
            None
        }
    }

    /// Returns a vector of neighbor positions for the given position.
    ///
    /// # Examples
    ///
    /// ```
    /// use boggle::Board;
    /// use boggle::Dimensions;
    ///
    /// let dimensions = Dimensions { rows: 3, cols: 3 };
    /// let board = Board::new(dimensions);
    /// let neighbors = board.neighbors(3);
    /// assert_eq!(neighbors, [0, 4, 6]);
    /// ```
    pub fn neighbors(&self, start: usize) -> Vec<usize> {
        let candidates = [
            self.up(start),
            self.left(start),
            self.right(start),
            self.down(start),
        ];

        // filter_map to unwrap the Somes
        candidates.into_iter().filter_map(|&x| x).collect()
    }

    pub fn char_at(&self, pos: usize) -> char {
        self.board[pos]
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for row in self.board.as_slice().chunks(self.dimensions.cols as usize) {
            writeln!(f, "{}", row.into_iter().collect::<String>())?;
        }
        Ok(())
    }
}
