//! Toy boggle puzzle generator and solver.

mod board;
mod dimensions;
mod dictionary;
mod solver;

pub use board::Board;
pub use dimensions::Dimensions;
pub use dictionary::Dictionary;
pub use solver::solve;
