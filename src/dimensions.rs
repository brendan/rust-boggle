//! Representation of a pair of integers as dimensions.
//!
//! Parses strings of the form `u32`x`u32` into a Dimension.
//!
//! # Examples
//! ```
//! use boggle::Dimensions;
//!
//! let dims: Dimensions = "3x4".parse().unwrap();
//!
//! assert_eq!(dims, Dimensions { rows: 3, cols : 4 });
//! ```

use std::fmt;
use std::num;
use std::str::FromStr;

#[derive(Debug)]
pub enum DimensionsParseError {
    ParseIntError(num::ParseIntError),
    WrongDimensionsError(usize),
}

use self::DimensionsParseError::*;

impl fmt::Display for DimensionsParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &WrongDimensionsError(n) => {
                write!(f, "expected 2 dimensions, found {}", n)
            }
            &ParseIntError(ref err) => write!(f, "{}", err),
        }
    }
}

impl From<num::ParseIntError> for DimensionsParseError {
    fn from(err: num::ParseIntError) -> Self {
        ParseIntError(err)
    }
}

#[derive(Debug,PartialEq,Copy,Clone)]
pub struct Dimensions {
    pub rows: usize,
    pub cols: usize,
}

impl FromStr for Dimensions {
    type Err = DimensionsParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let dims = s.split('x')
            .take(3)
            .map(|x| x.parse::<usize>())
            .collect::<Result<Vec<_>, _>>()?;
        match dims.len() {
            2 => Ok(Dimensions {
                rows: dims[0],
                cols: dims[1],
            }),
            n => Err(WrongDimensionsError(n)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_valid_dimensions() {
        let dims: Dimensions = "3x4".parse().unwrap();

        assert_eq!(dims, Dimensions { rows: 3, cols : 4 });
    }

    #[test]
    fn parse_empty_dimensions() {
        let dims = "".parse::<Dimensions>();

        assert!(dims.is_err());
    }

    #[test]
    fn parse_invalid_dimensions() {
        let dims = "-3x4".parse::<Dimensions>();

        assert!(dims.is_err());
    }

    #[test]
    fn parse_extra_dimensions() {
        let dims = "3x4x5".parse::<Dimensions>();

        assert!(dims.is_err());
    }
}
