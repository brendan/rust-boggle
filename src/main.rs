extern crate structopt;
#[macro_use]
extern crate structopt_derive;

extern crate boggle;

use structopt::StructOpt;

use boggle::{solve, Board, Dictionary, Dimensions};

#[derive(StructOpt, Debug)]
#[structopt]
struct Opt {
    #[structopt(short = "d", help = "board dimensions (<rows>x<cols>)")] dimensions: Dimensions,
    #[structopt(short = "n", help = "minimum valid word length")] minlen: usize,
    #[structopt(help = "dictionary file")] dictfile: String,
}

fn main() {
    let opt = Opt::from_args();

    let dict = Dictionary::from_file(&opt.dictfile).unwrap();
    let board = Board::new(opt.dimensions);

    println!("{}", board);

    for solution in solve(&board, &dict).filter(|ref word| word.len() >= opt.minlen) {
        println!("{}", solution);
    }
}
