extern crate itertools;

use self::itertools::Itertools;

use Board;
use Dictionary;

struct SearchState {
    // vector of positions forming a path from start to end
    path: Vec<usize>,
}

impl SearchState {
    pub fn new(pos: usize) -> SearchState {
        SearchState { path: vec![pos] }
    }

    pub fn push(&self, pos: usize) -> SearchState {
        let mut path = self.path.clone();
        path.push(pos);

        SearchState { path }
    }

    pub fn pos(&self) -> usize {
        *self.path.last().unwrap()
    }
}

pub struct SolveIterator<'a> {
    board: &'a Board,
    dict: &'a Dictionary,
    searchqueue: Vec<SearchState>,
}

impl<'a> SolveIterator<'a> {
    /// Initialize a new solution iterator
    pub fn new(board: &'a Board, dict: &'a Dictionary) -> SolveIterator<'a> {
        let searchqueue: Vec<SearchState> =
            (0..board.len()).map(|pos| SearchState::new(pos)).collect();

        SolveIterator {
            board,
            dict,
            searchqueue,
        }
    }
}

/// Return an iterator of words in `board`.
pub fn solve<'a>(board: &'a Board, dict: &'a Dictionary) -> itertools::Unique<SolveIterator<'a>> {
    SolveIterator::new(board, dict).unique()
}

fn letters_of_path(board: &Board, path: &Vec<usize>) -> String {
    path.iter().map(|&pos| board.char_at(pos)).collect()
}

// start with a queue of positions to search, and bfs
// needs an iterator adapter around it to filter out duplicates
impl<'a> Iterator for SolveIterator<'a> {
    type Item = String;

    /// Iterate over each solution found for the board
    fn next(&mut self) -> Option<String> {
        // loop over searchqueue until we find a word or run out of candidates
        while let Some(searchstate) = self.searchqueue.pop() {
            let mut word = letters_of_path(self.board, &searchstate.path);

            // add surrounding available squares to search queue
            for neighbor in self.board.neighbors(searchstate.pos()) {
                word.push(self.board.char_at(neighbor));
                if !searchstate.path.contains(&neighbor) && self.dict.has_prefix(&word) {
                    let nextstate = searchstate.push(neighbor);
                    self.searchqueue.push(nextstate);
                }
                word.pop();
            }
            if self.dict.has(&word) {
                return Some(word);
            }
        }
        None
    }
}
