//! A dictionary of words.
//!
//! # Examples
//! ```
//! use boggle::Dictionary;
//!
//! let words = vec!["arc", "arch", "bridge"];
//!
//! let dict: Dictionary = words.into_iter().map(|s| s.to_string()).collect();
//! assert!(dict.has_prefix("a"));
//! assert!(dict.has_prefix("bridge"));
//! assert!(!dict.has_prefix("car"));
//! ```

use std;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::iter::FromIterator;

pub struct Dictionary {
    words: Vec<String>,
}

impl Dictionary {
    /// Creates a dictionary from the file given by filename.
    /// The file is expected to be sorted with one word per line.
    pub fn from_file(filename: &str) -> Result<Dictionary, std::io::Error> {
        let f: File = File::open(filename)?;
        let f = BufReader::new(f);

        let words = f.lines().collect::<Result<Vec<_>, _>>()?;

        Ok(Dictionary { words })
    }

    /// Return true if prefix is a prefix of any word in the dictionary
    pub fn has_prefix(&self, prefix: &str) -> bool {
        // XXX why should I have to allocate prefix?
        match self.words.binary_search(&prefix.to_string()) {
            // prefix is a word
            Ok(_) => true,
            // the word following prefix starts with prefix
            Err(pos) if pos + 1 < self.words.len() && self.words[pos + 1].starts_with(prefix) => {
                true
            }
            _ => false,
        }
    }

    /// returns true if `word` is in dictionary
    pub fn has(&self, word: &str) -> bool {
        self.words.binary_search(&word.to_string()).is_ok()
    }
}

impl FromIterator<String> for Dictionary {
    fn from_iter<I: IntoIterator<Item=String>>(iter: I) -> Self {
        let words: Vec<String> = iter.into_iter().collect();

        Dictionary { words }
    }
}
